var express		= require("express"),
	boltDB		= require("boltDB"),
	bodyParser	= require("body-parser"),
	app			= express(),
	userData	= boltDB.get("data/userData.json", "userData");

app.set("view engine", "ejs");
app.use(bodyParser());


app.get("/", function(req, res) {
	res.render("index", userData);
});

app.post("/countBegMoney", function(req, res) {
	var begMoney = req.body.begMoney;
	var balance = userData.balance;
	balance = parseInt(balance);
	balance = balance + begMoney;
	boltDB.update("data/userData.json", "userData.balance", balance);
	res.render("index");
});

app.listen(3000);
